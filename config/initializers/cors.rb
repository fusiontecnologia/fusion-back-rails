    # Read more: https://github.com/cyu/rack-cors
    # Adicionado - Compatível devise_token_auth
    Rails.application.config.middleware.insert_before 0, Rack::Cors do
      allow do
        origins '*'

        resource '*',
          headers: :any,
          expose: ['access-token', 'expiry', 'token-type', 'uid', 'client'],    
          methods: [:get, :post, :put, :patch, :delete, :options, :head]
      end
    end
