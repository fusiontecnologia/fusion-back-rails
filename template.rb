require "fileutils"
require "shellwords"

def notify(msg, color)
  say "*********************************************************", color
  say 
  say "*** " + msg + " ***", color
  say
  say "*********************************************************", color

end

# O projeto tem que estar local entao tem que clonar temporariamente.
# Aqui seta diretório raiz para copiar diretamente alguns arquivos pelo comando.
def add_template_repository_to_source_path
  notify("Adquirindo referências online do repositório...", :blue)
  if __FILE__ =~ %r{\Ahttps?://}
    require "tmpdir"
    source_paths.unshift(tempdir = Dir.mktmpdir("fusion_back-"))
    at_exit { FileUtils.remove_entry(tempdir) }
    git clone: [
      "--quiet",
      "https://lucasfrdgs@bitbucket.org/lucasfrdgs/back.git",
      tempdir
    ].map(&:shellescape).join(" ")

    if (branch = __FILE__[%r{fusion_back/(.+)/template.rb}, 1])
      Dir.chdir(tempdir) { git checkout: branch }
    end
  else
    source_paths.unshift(File.dirname(__FILE__))
  end
end

def setup_devise_token_auth

  notify("Preparando as configurações do devise_token_auth...", :blue)

  gem 'devise_token_auth' 
  run 'bundle install'
  generate 'devise_token_auth:install User auth'

  notify("Adicionando extensão de Models do devise...", :magenta)

  insert_into_file(Dir["app/models/user.rb"].first,
    "\n  extend Devise::Models",
    after: "class User < ActiveRecord::Base"
  )
  notify("Você pode descomentar parâmetros de configuração do Devise no model...", :cyan)


  notify("Viabilizando uso do Trackable com devidos campos na migration", :magenta)
  insert_into_file(Dir["db/migrate/**/*devise_token_auth_create_users.rb"].first,
    "## Trackable
    t.integer  :sign_in_count, default: 0, null: false
    t.datetime :current_sign_in_at
    t.datetime :last_sign_in_at
    t.inet     :current_sign_in_ip
    t.inet     :last_sign_in_ip",
    before: "## Lockable"
  )

  rails_command "db:migrate"

  notify("Adicionando permissão para signup com o campo :name", :magenta)
  append_to_file "app/controllers/application_controller.rb",
    :after => "include DeviseTokenAuth::Concerns::SetUserByToken\n" do
    <<-RUBY
      before_action :configure_permitted_parameters

      protected

      def configure_permitted_parameters
        devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
      end

    RUBY
  end

  initializer 'devise.rb' do
    <<-EOF
    Devise.setup do |config|
      # The e-mail address that mail will appear to be sent from
      # If absent, mail is sent from "please-change-me-at-config-initializers-devise@example.com"
      config.mailer_sender = "support@fusiontecnologia.com"

      # If using rails-api, you may want to tell devise to not use ActionDispatch::Flash
      # middleware b/c rails-api does not include it.
      # See: https://stackoverflow.com/q/19600905/806956
      config.navigational_formats = [:json]
    end
    EOF
  end

  route "mount_devise_token_auth_for 'User', at: 'auth'"
  
end

def setup_rack_cors
  notify("Preparando Rack Cors...", :blue)
  gem 'rack-cors'
  run 'bundle'
  remove_file 'config/initializers/cors.rb'
  create_file 'config/initializers/cors.rb', <<-EOF
    # Read more: https://github.com/cyu/rack-cors
    # Adicionado - Compatível devise_token_auth
    Rails.application.config.middleware.insert_before 0, Rack::Cors do
      allow do
        origins '*'

        resource '*',
          headers: :any,
          expose: ['access-token', 'expiry', 'token-type', 'uid', 'client'],    
          methods: [:get, :post, :put, :patch, :delete, :options, :head]
      end
    end
  EOF
end

def setup_mailcatcher
  notify("Adicionando Mailcatcher...", :blue)

  run 'gem install mailcatcher'
  environment "config.action_mailer.default_url_options = { host: 'localhost', port: 3000 }" , env: 'development'
  environment "config.action_mailer.delivery_method = :smtp", env: 'development'
  environment "config.action_mailer.smtp_settings = { :address => 'localhost', :port => 1025 }" , env: 'development'

end

def setup_rolify
  notify("Inserindo Rolify...", :blue)
  notify("Para models que terão relação monitorada para User Rolify defina a diretiva resourcify no arquivo model.rb", :magenta)
  gem 'rolify'
  gem 'cancancan'
  run 'bundle'

  generate "rolify Role User" 
  insert_into_file(
    Dir["db/migrate/**/*rolify_create_roles.rb"].first,
    "[6.0]",
    after: "ActiveRecord::Migration"
  )

  
  rails_command "db:migrate"  

  userRules = 
  <<-RUBY
    attribute :rules
    def rules
      Ability.new(self).to_list
    end

    after_find do |user|
      user.rules = rules
      user.save
    end
  RUBY

  inject_into_file 'app/models/user.rb', userRules + "\n", :before => /^end/

  notify("Clonando o ability.rb do master...", :magenta)
  copy_file "app/models/ability.rb"

end


def setup_seeds
  notify("Copiando o seeds e criando usuários. Confira em seeds.rb", :blue)
  copy_file "db/seeds.rb"
  rails_command "db:seed"
end

def setup_graphql
  notify("Ainda não implementamos GraphQL no template...", :red)
end



# Main
run 'bundle'

add_template_repository_to_source_path

rails_command "db:create"
# rails_command(setup_devise_token_auth) if yes?("Deseja utilizar devise_token_auth? (y/n)")
if yes? "Deseja utilizar devise_token_auth? (y/n)", :blue
  setup_devise_token_auth
else
  notify("Foi mal, não temos opção ainda.. vamos de devise_token_auth!", :red)
  setup_devise_token_auth
end

setup_rack_cors
setup_mailcatcher

if yes? "Deseja utilizar graphQL? (y/n)", :blue
  setup_graphql
end

setup_rolify
setup_seeds

after_bundle do
  say
  say "Novo projeto template instalado com sucesso!!", :blue
  say
  say "Já comece programando o app, em breve teremos um README.txt como apoio de startup.", :green
  say "cd #{app_name} - Acessar o repositório."
  say "rails s -b 0.0.0.0 (subir e testar)"
end
