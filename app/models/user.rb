# frozen_string_literal: true

class User < ActiveRecord::Base
  rolify
  extend Devise::Models
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable
  include DeviseTokenAuth::Concerns::User
    attribute :rules
    def rules
      Ability.new(self).to_list
    end

    # after_find do |user|
    #   user.rules = rules
    #   user.save
    # end

end
