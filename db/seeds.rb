user_default = User.create!(provider: 'email', password: 123456, password_confirmation: 123456, email: 'user@login.com', name: 'Usuário de Oliveira')
user_fusion = User.create!(provider: 'email', password: 123456, password_confirmation: 123456, email: 'user@fusion.com', name: 'FUSION Tecnologia')

user_fusion.add_role :super_user
user_default.add_role :admin