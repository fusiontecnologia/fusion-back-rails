# Ruby on Rails Back-End

Fusion Tecnologia

Favor utilizar pull-requests..

## Utilização do template

Para iniciar um novo projeto com este template acesse o arquivo ./template.rb deste repositório.
Acesse o raw e copie o link.

crie uma pasta com o nome do projeto e acesse pelo terminal.

```
rails new nome-do-projeto --api -d postgresql -m https://www. + link-do-arquivo-raw
(não esquecer de colocar o 'https://www.')
```

### Recursos

- Ruby version - 2.7.0
- Rails 6.0
- api-only
- postgresql

* -
* Devise Token Auth
* Devise extends
* Rolify + CanCanCan
* Rack-cors
* seeds

#### Próximos passos:

- GraphQL

* ...

### Rodar

```
rails s -b 0.0.0.0
```

O sistema nasce com database migrado com usuários
user@fusion.com - 123456 (role: super_user)
user@login.com - 123456 (role: admin)
